@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Last 10 Blog Posts</div>

                    <div class="card-body">
                        @foreach($posts as $post)
                            <div>
                                <h2>{{$post->title}}</h2>
                                <img alt="{{$post->title}}" src="{{url('/uploads/'.$post->image)}}" style="max-width:150px">
                                <p>{{$post->content}}</p>
                                <p>{{$post->user['name']}}</p>
                                <a href="{{url('/post/'.$post->id)}}" class="btn btn-outline-dark">view post</a>
                                <br>
                                <h4>Comments:</h4>
                                @if(count($post->comments))
                                @foreach($post->comments as $comment)
                                    <p>{{$comment['name']}}:</p>
                                    <p>{{$comment['content']}}</p>
                                    <br>
                                    @endforeach
                                @else
                                    <p>no Comment</p>
                                @endif
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
