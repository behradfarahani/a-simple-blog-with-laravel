@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">All Blog Posts</div>

                    <div class="card-body">
                        @foreach($posts as $post)
                            <div>
                                <h2>{{$post->title}}</h2>
                                <img alt="{{$post->title}}" src="{{url('/uploads/'.$post->image)}}" style="max-width:150px">
                                <p>{{$post->content}}</p>
                                <p>{{$post->user['name']}}</p>
                                <a href="{{url('/post/'.$post->id)}}" class="btn btn-outline-dark">view post</a>
                            </div>
                            <hr>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
