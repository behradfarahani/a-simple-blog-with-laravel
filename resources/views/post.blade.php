@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{$post->title}}</div>

                    <div class="card-body">
                        <img alt="{{$post->title}}" src="{{url('/uploads/'.$post->image)}}" style="max-width:550px">

                        <p>{{$post->content}}</p>
                        <hr>
                        <p>{{ __('Author:') }} {{$post->user['name']}}
                            @auth
                            <a style="margin-left: 20px" href="{{url('/delete/'.$post->id)}}"><i class="fa fa-trash-alt"></i></a>
                            <a style="margin-left: 10px" href="{{url('/edit/'.$post->id)}}"><i class="fa fa-pen"></i></a>
                            @endauth
                        </p>
                        <br>
                        <br>
                        <h4 class="text-center">{{ __('Write a comment') }}</h4>
                        <form method="POST" action="{{ route('sendComment') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Your name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autofocus>
                                    <input id="post" type="hidden"  name="post" value="{{$post->id}}">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="content" class="col-md-4 col-form-label text-md-right">{{ __('Comment') }}</label>

                                <div class="col-md-6">
                                    <textarea id="content" rows="4" class="form-control @error('content') is-invalid @enderror" name="content" >{{ old('content') }}</textarea>
                                    @error('content')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Send Comment') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <hr>
                        <h3>{{ __('Comments') }}</h3>
                        @foreach($comments as $comment)
                            <div>
                                <h4>{{$comment->name}}:</h4>
                                <P>{{$comment->content}}</P>
                            </div>
                            <br>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
