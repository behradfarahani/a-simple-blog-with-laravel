<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function sendComment(Request $request){
        $request->validate([
            'name' => 'required|max:256',
            'content' => 'required|max:1000',
            'post' => 'required',
        ]);
        $name = $request->input('name');
        $content = $request->input('content');
        $post = $request->input('post');
        $is_post_correct = Post::where('id',$post)->count();
        if($is_post_correct){
            $comment = New Comment;
            $comment->name = $name;
            $comment->content = $content;
            $comment->post_id = $post;
            $comment->save();
            return redirect(url('post/'.$post));
        }else{
            abort(404);
        }

    }
}
