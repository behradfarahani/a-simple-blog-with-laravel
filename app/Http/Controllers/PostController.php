<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    public function sendPost(Request $request){
        $request->validate([
            'title' => 'required|max:256',
            'content' => 'required|max:1000',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:500',
        ]);
        $title = $request->input('title');
        $content = $request->input('content');
        $image= $request->file('image');
        $img=Image::make($image);
        $filename= date('U') . '.' . $image->getClientOriginalExtension();
        $img->save(public_path('/uploads/' . $filename));
        $post = New Post;
        $post->title = $title;
        $post->content = $content;
        $post->image = $filename;
        $post->user_id = Auth::user()->id;
        $post->save();
        return redirect(url('posts'));
    }
    public function showPosts(){
        $posts = Post::with('user')->get();
        return view('posts',['posts' => $posts]);
    }
    public function showPost($id){
        $post = Post::where('id',$id)->with('user')->first();
        $comments = Comment::where('post_id',$id)->get();
        return view('post',['post' => $post,'comments' => $comments]);
    }
    public function lastPosts(){
        $posts = Post::with('comments','user')->orderBy('id','DESC')->limit(10)->get();
        return view('lastPosts',['posts' => $posts]);
    }
    public function deletePost($id){
        $delete = Post::where('id',$id)->delete();
        if($delete){
            return redirect(url('/posts'));
        }else{
            abort(404);
        }
    }
    public function editPost($id){
        $post = Post::where('id',$id)->first();
        return view('edit',['post' => $post]);
    }
    public function sendEditPost(Request $request){
        $request->validate([
            'title' => 'required|max:256',
            'content' => 'required|max:1000',
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:500',
        ]);
        $title = $request->input('title');
        $content = $request->input('content');
        $post_id = $request->input('post');
        $is_post_correct = Post::where('id',$post_id)->count();
        if($is_post_correct){
            $post = Post::find($post_id);
            if($request->hasFile('image')){
                $image= $request->file('image');
                $img=Image::make($image);
                $filename= date('U') . '.' . $image->getClientOriginalExtension();
                $img->save(public_path('/uploads/' . $filename));
                $post->image = $filename;

            }
            $post->title = $title;
            $post->content = $content;
            $post->save();
            return redirect(url('post/'.$post_id));
        }else{
            abort(404);
        }
        abort(404);
    }
}
