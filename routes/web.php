<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/create', function () { return view('create'); });
    Route::post('/create','PostController@sendPost')->name('sendPost');
    Route::post('/edit','PostController@sendEditPost')->name('SendEditPost');
    Route::get('/delete/{id}','PostController@deletePost')->name('deletePost');
    Route::get('/edit/{id}','PostController@editPost')->name('editPost');
});

Route::get('/posts','PostController@showPosts')->name('showPosts');
Route::get('/post/{id}','PostController@showPost')->name('showPost');
Route::post('/sendComment','CommentController@sendComment')->name('sendComment');

Route::get('/lastPosts','PostController@lastPosts')->name('lastPosts');

