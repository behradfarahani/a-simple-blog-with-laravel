<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## How To run Project
In main Directory there is a file 'blog.sql' which is exported database file.
you can import it and start using the 'Simple Laravel Blog'.

## Author
[behradfarahani.ir](https://behradfarahani.ir)

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
